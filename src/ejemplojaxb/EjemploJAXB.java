/*
 * Ejemplo de JAXB
 */
package ejemplojaxb;

import generated.Libros;
import generated.Libros.Libro;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author Linkia
 */
public class EjemploJAXB {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Libros misLibros = new Libros();
            BufferedReader in = new BufferedReader(new FileReader("libros.txt"));
            Libro libroActual;
            String linea;
            while ((linea = in.readLine()) != null) {
                libroActual = new Libro();
                libroActual.setPublicadoEn(linea);
                linea = in.readLine();
                libroActual.setTitulo(linea);
                linea = in.readLine();
                libroActual.setAutor(linea);
                linea = in.readLine();
                libroActual.setEditorial(linea);
                misLibros.getLibro().add(libroActual);
            }
            File miXML = new File("libros.xml");
            JAXBContext contexto = JAXBContext.newInstance(Libros.class);
            Marshaller m = contexto.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(misLibros, miXML);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EjemploJAXB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EjemploJAXB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(EjemploJAXB.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
